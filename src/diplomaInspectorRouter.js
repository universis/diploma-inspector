import { Router } from 'express';
import * as packageJson from '../package.json';
import {validate} from './InspectItemRequest';
import { TraceUtils, Args, HttpBadRequestError, HttpError, HttpNotFoundError, HttpUnauthorizedError, HttpConflictError } from '@themost/common';
import { ElotConverter } from '@universis/elot-converter';
import moment from 'moment';
import {NumberFormatter} from '@universis/number-format';
import passport from 'passport';
import BearerStrategy from 'passport-http-bearer';
// swagger
import path from 'path';
import swaggerUi from 'swagger-ui-express';
import YAML from 'yamljs'; 
import {URL} from 'url';
import { ClientDataQueryable, ParserDataService } from '@themost/client';
import { HttpForbiddenError } from '@themost/common';
import ipRangeCheck from 'ip-range-check';
import onHeaders from 'on-headers';
import { cloneDeep } from 'lodash';

const DiplomaInspectorQuery = {
    "$project": {
        "id": 1,
        "graduationYear": 1,
        "graduationPeriod": 1,
        "graduationDate": 1,
        "declaredDate": 1,
        "lastObligationDate": 1,
        "graduationGrade": 1,
        "studentStatus": 1,
        "department": 1,
        "person": 1,
        "studyProgramSpecialty": 1,
        "studyProgram":1,
        "graduationGradeScale": 1,
        "departmentSnapshot": 1,
        "graduationTitle":1
    }
}

class DataFieldQueryResolver {

    /**
     * @param {import("./data-model").DataModel} target
     */
    constructor(target) {
        this.target = target;
    }

    nameReplacer(key, value) {
        let thisValue = value;
        if (value === 1) {
            thisValue = '$' + key;
        }
        if (typeof thisValue === 'string') {
            if (/^\$(\w+)$/.test(thisValue)) {
                const result = thisValue.replace(/^\$/, '');
                if (key !== result) {
                    return `${result} as ${key}`
                }
                return result;
            } else if (/^\$((\w+)(\.(\w+)){1,})$/.test(value)) {
                // eslint-disable-next-line no-useless-escape
                return value.replace(/^\$/, '').replace('\.', '/')
            }
        }
        return value;
    }
}


class PersonNotFoundError extends HttpNotFoundError {
    /**
     * @param {string=} msg 
     */
    constructor(msg) {
        super(msg || 'person not found');
        this.status = 404;
        this.code = '1050';
    }
}

class InvalidPayloadError extends HttpBadRequestError {
    /**
     * @param {string=} msg 
     */
    constructor(msg) {
        super(msg || 'cannot find person because of insufficient parameters');
        this.status = 400;
        this.code = '1000';
    }
}

class DegreeNotFoundError extends HttpNotFoundError {
    /**
     * @param {string=} msg 
     */
    constructor(msg) {
        super(msg || 'degrees not found');
        this.status = 404;
        this.code = '1060';
    }
}

class InvalidClientCredentialsError extends HttpUnauthorizedError {
    constructor(msg) {
        super(msg || 'invalid credentials on authentication');
        this.status = 401;
        this.code = '1001';
    }
}

class AuthenticationHeaderMissingError extends HttpUnauthorizedError {
    constructor(msg) {
        super(msg || 'authentication header missing');
        this.status = 401;
        this.code = '1030';
    }
}

class InvalidOrExpiredTokenError extends HttpUnauthorizedError {
    constructor(msg) {
        super(msg || 'invalid/expired token');
        this.status = 401;
        this.code = '1040';
    }
}

class MoreThanOnePersonFoundError extends HttpConflictError {
    constructor(msg) {
        super(msg || 'more than one persons found');
        this.status = 409;
        this.code = '1070';
    }
}

function parseBasicAuthorization(value)
{
    try {
        if (typeof value !== 'string')
            return;
        //get authorization type (basic)
        const re = /\s?(Basic)\s+(.*)\s?/ig;
        const match = re.exec(value.replace(/^\s+/g,''));
        if (match) {
            //get authorization token
            const token = match[2];
            //decode token
            const buffer = new Buffer(token, 'base64');
            //get args e.g. username:password
            const matched = /(.*):(.*)/ig.exec(buffer.toString());
            if (matched) {
                return { userName:matched[1], userPassword:matched[2] };
            }
        }
    }
    catch(err) {
        TraceUtils.log(err);
    }
}

/**
 * @param {ClientDataQueryable} q 
 * @returns 
 */
 function getFilter(q) {
    const filter = q.getParams().$filter;
    if (filter == null) {
        return;
    }
    if (filter.length === 0) {
        return;
    }
    return '(' + filter + ')';
}
/**
 * @returns {Router}
 */
function inspectRouter() {
    const router = Router();
    router.post('/', (req, _res, next) => {
        try {
            const translateService = req.context.getApplication().getService(function TranslateService() {});
            const result = validate(req.body);
            TraceUtils.debug(`Validation: ${result}`);
            Args.check(result, new InvalidPayloadError(translateService.translate('cannot find person because of insufficient parameters')));
            return next();
        } catch (err) {
            return next(err);
        }
    }, async (req, res, next) => {
        try {
            const translateService = req.context.getApplication().getService(function TranslateService() {});
            const Students = req.context.model('Student');
            const filters = [];
            const stages = [];
            if (req.body.ssn  && req.body.ssn.length) {
                // stage-1: filter by social security number
                filters.push(
                    getFilter(
                        new ClientDataQueryable('Student', new ParserDataService()).where('person/SSN').equal(req.body.ssn)
                    )
                );
                stages.push('socialSecurityNumber');
            }
            if (req.body.taxId  && req.body.taxId.length) {
                // stage-2: filter by tax number
                filters.push(
                    getFilter(
                        new ClientDataQueryable('Student', new ParserDataService()).where('person/vatNumber').equal(req.body.taxId)
                    )
                );
                stages.push('taxNumber');
            }
            if (req.body.firstName && req.body.lastName && req.body.fatherName && req.body.motherName && req.body.birthDate) {
                // stage-3: filter by personal data
                const reqBirthDate = new Date(req.body.birthDate);
                filters.push(
                    getFilter(
                        new ClientDataQueryable('Student', new ParserDataService())
                            .where('person/givenName').indexOf(req.body.firstName.substring(0,1)).equal(0)
                            .and('person/familyName').indexOf(req.body.lastName.substring(0,1)).equal(0)
                            .and('person/fatherName').indexOf(req.body.fatherName.substring(0,1)).equal(0)
                            .and('person/motherName').indexOf(req.body.motherName.substring(0,1)).equal(0)
                            .and('person/birthDate').getFullYear().equal(reqBirthDate.getFullYear())
                            .and('person/birthDate').getMonth().equal(reqBirthDate.getMonth() + 1)
                            .and('person/birthDate').getDay().equal(reqBirthDate.getDate())
                    )
                );
                stages.push('personalInformation');
            }
            if (filters.length === 0) {
                throw new InvalidPayloadError(translateService.translate('cannot find person because of insufficient parameters'));
            }
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await new Promise((resolve, reject) => {
                Students.filter(filters.join(' or '), (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(result);
                });
            });

            // validate diploma inspector scope
            const inspectorScope = req.context.getApplication().getConfiguration().getSourceAt('settings/universis/diploma-inspector/scope') || 'diploma-inspector';
            // set silent mode for service to service authorization
            const serviceMode = !!(req.context.user && req.context.user.authenticationScope === inspectorScope);
            // get student(s)
            let inspectorQuery = req.context.getApplication().getConfiguration().getSourceAt('settings/universis/diploma-inspector/query');
            if (inspectorQuery && inspectorQuery.$project) {
                const project = Object.assign({}, DiplomaInspectorQuery.$project, inspectorQuery.$project);
                const resolver = new DataFieldQueryResolver(Students);
                const attributes = Object.keys(project).filter((key) => {
                    // exclude properties
                    if (Object.prototype.hasOwnProperty.call(project, key)) {
                        return project[key] !== 0;
                    }
                    return false;
                }).map((key) => {
                    const value =  project[key];
                    return resolver.nameReplacer(key, value);
                });
                query.select(attributes);
            }
            const items = await query.prepare().expand({
                    name: 'studentStatus'
                }, {
                    name: 'person',
                    options: {
                        $expand: 'gender,locales'
                    }
                }, {
                    name: 'department',
                    options: {
                        $expand: 'locales,dataAvailability,organization($expand=locales,dataAvailability)'
                    }
                }, {
                    name: 'studyProgramSpecialty',
                    options: {
                        $expand: 'locales'
                    }
                }, {
                    name: 'studyProgram',
                    options: {
                        $expand: 'studyLevel($expand=locales),info($expand=locales,specializationTitleType)'
                    }
                },{
                    name: 'graduationGradeScale',
                    options: {
                        $expand: 'locales,values($expand=locales)'
                    }
                },{
                    name: 'departmentSnapshot',
                    options: {
                        $expand: 'locales'
                    }
                })
                .silent(serviceMode).getItems();
            // validate department snapshot
            items.forEach((item) => {
                // add certifier
                item.certifier = cloneDeep(item.department.organization);
                if (item.departmentSnapshot) {
                    Object.assign(item.department, {
                        name: item.departmentSnapshot.name || item.department.name,
                        facultyName: item.departmentSnapshot.facultyName || item.department.facultyName
                    });
                    Object.assign(item.department.organization, {
                        name: item.departmentSnapshot.instituteName || item.department.organization.name,
                    });
                    // set department snapshot locale
                    const departmentSnapshotLocale =  item.departmentSnapshot.locales.find((x) => x.inLanguage === 'en');
                    if (departmentSnapshotLocale) {
                        // get department locale
                        const departmentLocale = item.department.locales.find((x) => x.inLanguage === 'en');
                        if (departmentLocale == null) {
                            item.department.locales.push(departmentSnapshotLocale);
                        } else {
                            Object.assign(departmentLocale, {
                                name: departmentSnapshotLocale.name || departmentLocale.name,
                                facultyName: departmentSnapshotLocale.facultyName || departmentLocale.facultyName
                            });
                        }
                        const organizationLocale = item.department.organization.locales.find((x) => x.inLanguage === 'en');
                        if (organizationLocale) {
                            Object.assign(organizationLocale, {
                                name: departmentSnapshotLocale.instituteName || organizationLocale.name
                            });
                        } else {
                            item.department.organization.locales.push({
                                name: departmentSnapshotLocale.instituteName
                            });
                        }
                    }
                }
            });
            let locale = req.context.getApplication().getConfiguration().getSourceAt('settings/universis/diploma-inspector/defaultLocale');
            if (locale == null) {
                locale = req.context.getApplication().getConfiguration().getSourceAt('settings/i18n/defaultLocale');
            }

            const intermediateResults = items.filter((item) => {
                if (stages.indexOf('personalInformation') < 0) {
                    return true;
                }
                //  this is the last stage of filtering data based on first-letters matching of personal information
                if (
                    String(item.person.familyName).substring(0, 5).toLocaleUpperCase(locale) === String(req.body.lastName).substring(0, 5).toLocaleUpperCase(locale) &&
                    String(item.person.givenName).substring(0, 5).toLocaleUpperCase(locale) === String(req.body.firstName).substring(0, 5).toLocaleUpperCase(locale) &&
                    String(item.person.fatherName).substring(0, 3).toLocaleUpperCase(locale) === String(req.body.fatherName).substring(0, 3).toLocaleUpperCase(locale) &&
                    String(item.person.motherName).substring(0, 3).toLocaleUpperCase(locale) === String(req.body.motherName).substring(0, 3).toLocaleUpperCase(locale)) {
                    return true;
                }
                return false;
            });

            // validate more than one person
            if (intermediateResults.length > 0) {
                // validate unique value of social security number
                // important note: this operation validates data returned by the query
                // without taking in count the supplied parameters
                let validateValues = intermediateResults.filter((item) => {
                    // exclude empty values
                    return item.person.SSN != null && item.person.SSN.length > 0;
                }).map((item) => item.person.SSN);
                let uniqueValues = [...new Set(validateValues)];
                // if result set contains more than one values
                if (uniqueValues.length > 1) {
                    // throw error
                    throw new MoreThanOnePersonFoundError(translateService.translate('more than one persons found'));
                }
                // validate unique value of tax number
                validateValues = intermediateResults.filter((item) => {
                    // exclude empty values
                    return item.person.vatNumber != null && item.person.vatNumber.length > 0;
                }).map((item) => item.person.vatNumber);
                uniqueValues = [...new Set(validateValues)];
                // if result set contains more than one values
                if (uniqueValues.length > 1) {
                    // throw error
                    throw new MoreThanOnePersonFoundError(translateService.translate('more than one persons found'));
                }
            }

            const results = intermediateResults.filter((item) => {
                return item.studentStatus.alternateName === 'graduated';
            }).map((item) => {
                // set last obligation date
            const lastObligationDate = item.lastObligationDate || item.declaredDate || item.graduationDate;
            const gradeScale = req.context.model('GradeScale').convert(item.graduationGradeScale);
            let graduationGrade = gradeScale.convertTo(item.graduationGrade, true);
            let graduationGradeEn = graduationGrade;
            let gradeText = graduationGrade;
            let gradeTextEn = null;
            let degreeGradeTitle = null;
            // if grade scale is numeric
            if (gradeScale.scaleType === 0) {
                // get fraction digits from study program
                let fractionDigits = item.studyProgram.decimalDegits;
                if (fractionDigits == null) {
                    // or grade scale
                    fractionDigits = gradeScale.formatPrecision;
                }
                // format grade as text
                const gradeAsNumber = Number(graduationGrade.replace(',','.'));
                gradeText =  new NumberFormatter().format(gradeAsNumber, locale, fractionDigits);
                // try to get grade text in english
                gradeTextEn = new NumberFormatter().format(gradeAsNumber, 'en', fractionDigits);
                // get grade value text
                if (Array.isArray(gradeScale.values)) {
                    const gradeScaleValue = gradeScale.values.find((value) => {
                        return (value.valueFrom <= item.graduationGrade) && (value.valueTo >= item.graduationGrade);
                    });
                    if (gradeScaleValue) {
                        degreeGradeTitle = gradeScaleValue.name;
                    }
                }
            } else if (gradeScale.scaleType === 1) {
                graduationGradeEn = gradeTextEn = null;
                if (Array.isArray(gradeScale.values)) {
                    const gradeScaleValue = gradeScale.values.find((value) => {
                        return (value.valueFrom <= item.graduationGrade) && (value.valueTo >= item.graduationGrade);
                    });
                    if (gradeScaleValue) {
                       degreeGradeTitle = gradeScaleValue.name;
                       const gradeScaleValueEn = gradeScaleValue.locales.find((x) => x.inLanguage === 'en');
                       graduationGradeEn = gradeTextEn = gradeScaleValueEn && gradeScaleValueEn.name;
                    }
                }
            }

            let gradingScaleEn = null;
            if (gradeScale != null && Array.isArray(gradeScale.locales)) {
                const gradeScaleLocale = gradeScale.locales.find((x) => x.inLanguage === 'en');
                gradingScaleEn = gradeScaleLocale && gradeScaleLocale.name;
            }

            // get degree level for both languages
            const degreeLevel = item.studyProgram.studyLevel && item.studyProgram.studyLevel.name;
            let degreeLevelEn = null;
            if (item.studyProgram.studyLevel && Array.isArray(item.studyProgram.studyLevel.locales)) {
                const degreeLevelLocale = item.studyProgram.studyLevel.locales.find((x) => x.inLanguage === 'en');
                degreeLevelEn = degreeLevelLocale && degreeLevelLocale.name;
            }

            let orientation = item.studyProgramSpecialty && item.studyProgramSpecialty.name;
            // get birthPlaceEn
            let birthPlaceEn = null;
            const personEn = item.person.locales.find((x) => x.inLanguage === 'en');
            if (personEn && personEn.birthPlace) {
                birthPlaceEn = personEn.birthPlace
            } else {
                birthPlaceEn = item.person.birthPlace != null ? ElotConverter.convert(String(item.person.birthPlace).toLocaleUpperCase(locale)) : null;
            }
            // get institute en locale
            const institutionEn = item.department.organization.locales.find((x) => x.inLanguage === 'en');
            // get department en locale
            const departmentEn = item.department.locales.find((x) => x.inLanguage === 'en');
            let studyProgramEn = null;
            if (item.studyProgram && item.studyProgram.info) {
                studyProgramEn = item.studyProgram.info.locales.find((x) => x.inLanguage === 'en');
            }
            let availableSince = null;
            let availableSinceDescription = null;
            if (item.department && item.department.dataAvailability) {
                availableSince = item.department.dataAvailability.availableSince;
                availableSinceDescription = item.department.dataAvailability.availableSinceDescription;
            } else if (item.department && item.department.organization &&
                item.department.organization.dataAvailability) {
                availableSince = item.department.organization.dataAvailability.availableSince;
                availableSinceDescription = item.department.organization.dataAvailability.availableSinceDescription;
            }
            const certifierInstitutionEn = item.certifier.locales.find((x) => x.inLanguage === 'en');

            let orientationEn = item.studyProgramSpecialty && item.studyProgramSpecialty.locales.find((x) => x.inLanguage === 'en');
            // clear orientationEl and orientationEn when specialization type is general
            // difference between Major, Specialisation or Area Of Study
            // A general specialisation or an area of study may be used interchangeably
            // when referring to more comprehensive studies on a topic within a discipline
            if (item.studyProgram.info &&
                item.studyProgram.info.specializationTitleType &&
                item.studyProgram.info.specializationTitleType.alternateName === 'general') {
                    orientationEn = null;
                    orientation = null;
                }
            // return results
                return {
                    // unique id
                    id: `${item.department.id}/${item.graduationYear.id}/${item.graduationPeriod.id}/${item.id}`,
                    // data avalaible since
                    data_available_since: availableSince ? moment(availableSince).format('YYYY-MM-DD') : null,
                    // data avalaible since
                    data_availability_info: availableSinceDescription,
                    // social security number
                    ssn: item.person.SSN,
                    // tax number
                    taxId: item.person.vatNumber,
                    // last name
                    lastNameEl: item.person.familyName,
                    // ELOT last name
                    lastNameEn: ElotConverter.convert(item.person.familyName),
                    // first name
                    firstNameEl: item.person.givenName,
                    // ELOT first name
                    firstNameEn: ElotConverter.convert(item.person.givenName),
                    // father name
                    fatherNameEl: item.person.fatherName,
                    // father name (en)
                    fatherNameEn: ElotConverter.convert(item.person.fatherName),
                    // mother name
                    motherNameEl: item.person.motherName,
                    // mother name (en)
                    motherNameEn: ElotConverter.convert(item.person.motherName),
                    // birth date
                    birthDate: item.person.birthDate ? moment(item.person.birthDate).format('YYYY-MM-DD') : null,
                    // place of birth
                    birthPlaceEl: item.person.birthPlace != null ? String(item.person.birthPlace).toLocaleUpperCase(locale) : null,
                    // place of birth (en)
                    birthPlaceEn: birthPlaceEn,
                    // department identifier
                    departmentID:  item.department.alternativeCode,
                    // institute identifier
                    institutionID:  String(item.department.organization.id),
                    // degree identifier
                    degreeID:  `${item.department.id}/${item.studyProgram.id}/${(item.studyProgramSpecialty && item.studyProgramSpecialty.index) ? item.studyProgramSpecialty && item.studyProgramSpecialty.index + 1 : 0}`,
                    // certifier name
                    certifierInstitutionEl: (item.certifier && item.certifier.name) ? String(item.certifier && item.certifier.name).toLocaleUpperCase(locale) : null,
                    // certifier name (in english)
                    certifierInstitutionEn: (certifierInstitutionEn && certifierInstitutionEn.name) ? String(certifierInstitutionEn.name).toUpperCase() : null,
                    // institute name
                    institutionEl: String(item.department.organization && item.department.organization.name).toLocaleUpperCase(locale),
                    // institute name in english
                    institutionEn: (institutionEn && institutionEn.name) ? String(institutionEn.name).toUpperCase() : null,
                    // faculty 
                    schoolEl: item.department.facultyName != null ? String(item.department.facultyName).toLocaleUpperCase(locale) : null,
                    // faculty (en)
                    schoolEn: (departmentEn && departmentEn.facultyName) ? String(departmentEn.facultyName).toUpperCase(): null,
                    // department
                    departmentEl:  String(item.department.name).toLocaleUpperCase(locale),
                    // department (en)
                    departmentEn:  (departmentEn && departmentEn.name) ? String(departmentEn.name).toUpperCase(): null,
                    // title
                    degreeTitleEl: item.graduationTitle? String(item.graduationTitle).toLocaleUpperCase(locale): item.studyProgram && item.studyProgram.info && item.studyProgram.info.degreeDescription ? String(item.studyProgram.info.degreeDescription).toLocaleUpperCase(locale) : null,
                    // title (en)
                    degreeTitleEn: (studyProgramEn && studyProgramEn.degreeDescription) ? String(studyProgramEn.degreeDescription).toUpperCase() : null,
                    // grade e.g. 8,5 or PASSED
                    gradeEl: graduationGrade,
                    // grade (en)
                    gradeEn: graduationGradeEn,
                    // grade as text e.g. EIGHT AND FIVE TENTHS 
                    gradeTextEl: String(gradeText).toLocaleUpperCase(locale),
                    // grade as text (en)
                    gradeTextEn: gradeTextEn ? String(gradeTextEn).toLocaleUpperCase(locale) : null,
                    // grade scale
                    gradingScaleEl: String(gradeScale.name).toLocaleUpperCase(locale),
                    // grade scale (en)
                    gradingScaleEn: gradingScaleEn != null ? String(gradingScaleEn).toLocaleUpperCase(locale) : null,
                    // grade title e.g. EXCELLENT
                    degreeGradeTitle: degreeGradeTitle != null ? String(degreeGradeTitle).toLocaleUpperCase(locale): null,
                    // study level
                    degreeLevelEl: degreeLevel != null ? String(degreeLevel).toLocaleUpperCase(locale) : null,
                    // study level (en)
                    degreeLevelEn: degreeLevelEn != null ? String(degreeLevelEn).toLocaleUpperCase(locale) : null,
                    // specialization
                    orientationEl: orientation !=null ? String(orientation).toLocaleUpperCase(locale) : null,
                    // specialization
                    orientationEn: orientationEn && orientationEn.name ? String(orientationEn.name).toLocaleUpperCase(locale) : null,
                    // last obligation date
                    lastObligationDate: lastObligationDate ? moment(lastObligationDate).format('YYYY-MM-DD') : null,
                    // graduation date
                    graduationOathDate: item.graduationDate ? moment(item.graduationDate).format('YYYY-MM-DD') : null
                }
            }).filter((item) => {
                // get current context scopes
                const scope = req.context && req.context.user && req.context.user.authenticationScope;
                if (scope) {
                    // if scope "registrar" is valid for the current context
                    if (String(scope.replace(/(\s+)/g,',')).split(',').indexOf('registrar') >= 0) {
                        // do not filter data based on data availability attributes
                        return true;
                    }
                }
                // filter based on data_available_since attribute
                if (item.data_available_since) {
                    const date = item.graduationOathDate || item.lastObligationDate;
                    // if graduation date is empty
                    if (date == null) {
                        // do not return degreed
                        return false;
                    } else {
                        // otherwise, check if graduation is greater or equal to data available since
                        return date >= item.data_available_since;
                    }
                }
                return true;
            });
            if (results.length === 0) {
                if (intermediateResults.length > 0) {
                    throw new DegreeNotFoundError(translateService.translate('degrees not found'));
                }
                throw new PersonNotFoundError(translateService.translate('person not found'));
            }
            // format response
            return res.json({
                degrees: results
            });
        } catch (err) {
            return next(err);
        }
    }, async function handleError(err, req, res, next) {
        try {
            const institute = await req.context.model('Institute').where('local').equal(true)
                        .expand('dataAvailability', 'locales')
                        .silent().getItem();
            let data_available_since = institute && institute.dataAvailability && institute.dataAvailability.availableSince;
            let data_availability_info = institute && institute.dataAvailability && institute.dataAvailability.availableSinceDescription;
            const additionalData = {
                data_available_since: data_available_since ? moment(data_available_since).format('YYYY-MM-DD') : null,
                data_availability_info: data_availability_info,
                institutionEl: (institute && institute.name) || '',
                schoolEl: '',
                departmentEl: '',
                degreeTitleEl: ''
            };
            if (err instanceof PersonNotFoundError) {
                Object.assign(err, additionalData);
            } else if (err instanceof InvalidPayloadError) {
                Object.assign(err, additionalData);
            } else if (err instanceof DegreeNotFoundError) {
                Object.assign(err, additionalData);
            } else if (err instanceof MoreThanOnePersonFoundError) {
                Object.assign(err, additionalData);
            }
            return next(err);
        } catch (error) {
            return next(error);
        }
    });
    return router;
}

/**
 * @param {ApplicationBase} app
 * @returns {Router}
 */
// eslint-disable-next-line no-unused-vars
function diplomaInspectorRouter(app) {
    const router = Router();
    // get whitelist (which is optional by default)
    let whitelist = app.getConfiguration().getSourceAt('settings/universis/diploma-inspector/whitelist') || [];
    if (Array.isArray(whitelist) === false) {
        TraceUtils.warn('Services: @universis/diploma-inspector whilelist is expected to be a valid array of remote addresses.');
        // reset whitelist
        whitelist = [ '127.0.0.1' ];
    }
    if (whitelist.length === 0) {
        TraceUtils.warn('Services: @universis/diploma-inspector whilelist has not been set. The default whitelist ["127.0.0.1"] will be loaded instead.');
        whitelist = [ '127.0.0.1' ];
    }
    // get proxyAddressForwarding
    let proxyAddressForwarding = app.getConfiguration().getSourceAt('settings/universis/api/proxyAddressForwarding');
    if (typeof proxyAddressForwarding !== 'boolean') {
        proxyAddressForwarding = false;
    }

    // eslint-disable-next-line no-unused-vars
    function validateScopeHandler(req, res, next) {
        if (req.context == null) {
            return next(new HttpForbiddenError('Application context cannot be empty'));
        }
        try {
            const scopeAccess = req.context.getApplication().getConfiguration().getStrategy(function ScopeAccessConfiguration() {});
            if (scopeAccess != null) {
                return scopeAccess.verify(req).then((valid) => {
                    if (valid === false) {
                        return next(new HttpForbiddenError('Access denied due to authorization scopes.'));
                    }
                    return next();
                }).catch((error) => {
                    return next(error);
                });
            }
            return next();
        } catch (error) {
            return next(error);
        }
    }


    function noCacheHandler(req, res, next) {
        onHeaders(res, ()=> {
            res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            res.setHeader('Pragma', 'no-cache');
            res.setHeader('Expires', 0);
            // remove express js etag header
            res.removeHeader('ETag');
        });
        return next();
    }

    /**
     * @param {Re} req 
     * @param {*} res 
     * @param {*} next 
     * @returns 
     */
    function whitelistHandler(req, res, next) {
        if (whitelist.length === 0) {
            return next();
        }
        let remoteAddress = proxyAddressForwarding ? req.headers['x-forwarded-for'] : req.connection.remoteAddress;
        // handle multiple ip addresses
        let remoteAddresses = [];
        if (typeof remoteAddress === 'string') {
            remoteAddresses = remoteAddress.replace(/\s/g,'').split(',');
        }
        if (remoteAddresses.length === 0) {
            return next(new HttpBadRequestError('Invalid remote client'));
        }
        // get last remote address which is either a valid remote address without proxy
        // or the last proxy provided by x-forwarded-for header
        remoteAddress = remoteAddresses[remoteAddresses.length - 1];
        // validate remoteAddress
        if (ipRangeCheck(remoteAddress, whitelist) === false) {
            return next(new HttpForbiddenError());
        }
        return next();
    }

    passport.use(new BearerStrategy({
        passReqToCallback: true
        },
        /**
         * @param {Request} req
         * @param {string} token
         * @param {Function} done
         */
        function(req, token, done) {
            /**
             * Gets OAuth2 client services
             * @type {*}
             */
            const client = req.context.getApplication().getStrategy(function OAuth2ClientService() {});
            const translateService = req.context.getApplication().getService(function TranslateService() {});
            // if client cannot be found
            if (typeof client === 'undefined') {
                // throw configuration error
                return done(new Error('Invalid application configuration. OAuth2 client service cannot be found.'))
            }
            if (token == null) {
                // throw 499 Token Required error
                return done(new HttpError(499, 'A token is required to fulfill the request.', 'Token Required'));
            }
            // get token info
            client.getTokenInfo(req.context, token).then(info => {
                if (info == null) {
                    // the specified token cannot be found - 498 invalid token with specific code
                    return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
                }
                // if the given token is not active throw token expired - 498 invalid token with specific code
                if (!info.active) {
                    return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
                }
                const scope = app.getConfiguration().getSourceAt('settings/universis/diploma-inspector/scope') || 'diploma-inspector';
                // validate client credentials scope
                if (info.scope !== scope) {
                    return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
                }
                return done(null, {
                    "name": info.username,
                    "authenticationType":'Bearer',
                    "authenticationToken": token,
                    "authenticationScope": info.scope
                });
            }).catch(err => {
                const statusCode = (err && err.statusCode) || 500;
                if (statusCode === 404) {
                    // revert 404 not found returned by auth server to 498 invalid token
                    return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
                }
                // otherwise continue with error
                return done(err);
            });
        }
    ));
    // use this handler to create a router context
    router.use((req, _res, next) => {
        // create router context
        const newContext = app.createContext();
        Object.defineProperty(newContext, 'user', {
            enumerable: false,
            configurable: true,
            get: function() {
                return req.user;
            }
        });
        /**
         * try to find if request has already a data context
         * @type {ExpressDataContext|*}
         */
        const interactiveContext = req.context;
        // finalize already assigned context
        if (interactiveContext) {
            if (typeof interactiveContext.finalize === 'function') {
                // finalize interactive context
                return interactiveContext.finalize(() => {
                    // and assign new context
                    Object.defineProperty(req, 'context', {
                        enumerable: false,
                        configurable: true,
                        get: () => {
                            return newContext
                        }
                    });
                    // exit handler
                    return next();
                });
            }
        }
        // otherwise assign context
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
                return newContext
            }
        });
        // and exit handler
        return next();
    });
    router.use((req, _res, next) => {
        // set context locale from request
        req.context.locale  = req.locale;
        // set translate
        const translateService = req.context.application.getStrategy(function TranslateService() {});
        req.context.translate = function() {
            if (translateService == null) {
                return arguments[0];
            }
            return translateService.translate.apply(translateService, Array.from(arguments));
        };
        return next();
    });
    // use this handler to finalize router context
    // important note: context finalization is very important in order
    // to close and finalize database connections, cache connections etc.
    router.use((req, _res, next) => {
        req.on('end', () => {
            //on end
            if (req.context) {
                //finalize data context
                return req.context.finalize( () => {
                    //
                });
            }
        });
        return next();
    });

    function getSwaggerDocument() {
        try {
            const result = YAML.load(path.resolve(__dirname, './api-docs/swagger.yaml'));
            const origin = app.getConfiguration().getSourceAt('settings/universis/api/origin');
            if (origin) {
                result.servers = [
                    {
                        url: new URL('/services/diploma-inspector/', origin).toString()
                    }
                ]
            }
            Object.assign(result.info, {
                version: packageJson.version
            });
            return result;
        } catch (err) {
            TraceUtils.error(`(diplomaInspectorRouter) Swagger document cannot be loaded. Service documentation will be unavailable.`);
            TraceUtils.error(err);
        }
    }

    // get swagger document
    const swaggerDocument = getSwaggerDocument();
    // add route for swagger ui
    if (swaggerDocument) {

        router.use('/api-docs/schema', (_req, res) => {
            return res.json(swaggerDocument);
        });

        router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    }

    function authenticateHandler(req, res, next) {
        const service = req.context.getApplication().getService(function OAuth2ClientService() {});
        const translateService = req.context.getApplication().getService(function TranslateService() {});
        const scope = app.getConfiguration().getSourceAt('settings/universis/diploma-inspector/scope');
        const usernameAndPassword = parseBasicAuthorization(req.headers.authorization);
        if (usernameAndPassword == null) {
            return next(new AuthenticationHeaderMissingError(translateService.translate('authentication header missing')));
        }
        service.authorize({
            client_id: usernameAndPassword.userName,
            client_secret: usernameAndPassword.userPassword,
            grant_type: 'client_credentials',
            scope: scope || 'diploma-inspector'
        }).then((result) => {
            return res.json(result);
        }).catch((err) => {
            if (err.statusCode === 400 || err.statusCode === 401) {
                return next(new InvalidClientCredentialsError(translateService.translate('invalid credentials on authentication')));
            }
            return next(err);
        });
    }

    router.use(noCacheHandler);

    // POST /authenticate
    router.post('/authenticate', whitelistHandler, authenticateHandler);

    // POST /authenticate
    router.get('/authenticate', whitelistHandler, authenticateHandler);

    // POST /inspect
    router.use('/inspect', whitelistHandler, passport.authenticate('bearer', {session: false}), inspectRouter(app));

    return router;
}

export {
    PersonNotFoundError,
    InvalidPayloadError,
    DegreeNotFoundError,
    InvalidClientCredentialsError,
    InvalidOrExpiredTokenError,
    MoreThanOnePersonFoundError,
    AuthenticationHeaderMissingError,
    inspectRouter,
    diplomaInspectorRouter
}
