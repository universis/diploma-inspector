import Ajv from 'ajv';

const InspectItemRequestSchema = {
    type: 'object',
    properties: {
        taxId: {
            type: 'string',
            nullable: true,
            pattern: '^(\\d{9,9})$'
        },
        ssn: {
            type: 'string',
            nullable: true,
            pattern: '^(\\d{11,11})$'
        },
        firstName: {
            type: 'string',
            nullable: true
        },
        lastName: {
            type: 'string',
            nullable: true
        },
        fatherName: {
            type: 'string',
            nullable: true
        },
        motherName: {
            type: 'string',
            nullable: true
        },
        birthDate: {
            type: 'object',
            nullable: true
        }
    },
    required: [],
    additionalProperties: false
};

const validate = new Ajv().compile(InspectItemRequestSchema);

export {
    validate,
    InspectItemRequestSchema
}