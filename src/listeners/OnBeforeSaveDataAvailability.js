// eslint-disable-next-line no-unused-vars
const { DataEventArgs, DataObjectState } = require('@themost/data');

/**
 * @param {DataEventArgs} event 
 */
async function beforeSaveAsync(event) {
    const context = event.model.context;
    const property = 'dataAvailability';
    if (Object.prototype.hasOwnProperty.call(event.target, property) === true) {
        const propertyValue =  event.target[property];
        // get association mapping
        const mapping = event.model.inferMapping(property);
        // get child model
        const childModel = context.model(mapping.childModel);
        if (event.state === DataObjectState.Insert) {
            if (propertyValue != null) {
                // removee primary key
                delete propertyValue.id;
                // try to save child object
                await childModel.save(propertyValue);
            }
        } else if (event.state === DataObjectState.Update) {
            const target = event.model.convert(event.target);
            // get original value (that already exists)
            const originalValue = await target.property(property).silent().getItem();
            if (propertyValue === null) {
                // get value
                if (originalValue != null) {
                    // remove child object
                    await childModel.remove(originalValue);
                }
            } else {
                if (originalValue != null) {
                    Object.assign(propertyValue, {
                        id: originalValue.id
                    });
                    // update child object
                    await childModel.save(propertyValue); 
                } else {
                    // or insert child object
                    await childModel.insert(propertyValue); 
                }
            }
        }
    }
}

function beforeSave(event, callback) {
    beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

export {
    beforeSave
}