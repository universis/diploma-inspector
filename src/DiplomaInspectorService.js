import { ApplicationService, HttpForbiddenError } from '@themost/common';
import {ODataModelBuilder} from '@themost/data';
import { diplomaInspectorRouter, inspectRouter } from './diplomaInspectorRouter';
import { InstituteReplacer } from './InstituteReplacer';
import { DepartmentReplacer } from './DepartmentReplacer';
import { Router } from 'express';
const el = require('./locales/el.json');
/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
 function insertRouterBefore(parent, before, insert) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}

class DiplomaInspectorService extends ApplicationService {
    constructor(app) {
        super(app);
        new InstituteReplacer(app).apply();
        new DepartmentReplacer(app).apply();
        // refresh builder
        const builder = app.getService(ODataModelBuilder);
        if (builder != null) {
            // cleanup builder and wait for next call
            builder.clean(true);
            builder.initializeSync();
        }

        if (app && app.serviceRouter) {
            app.serviceRouter.subscribe((thisRouter) => {
                if (thisRouter != null) {
                    // add custom route
                    const addRoute = Router();
                    addRoute.use('/Students/InspectDegree', inspectRouter());
                    thisRouter.stack.unshift(...addRoute.stack);
                }
            });
        }

        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // append translations
                    const translateService = app.getService(function TranslateService() {});
                    if (translateService) {
                        translateService.setTranslation('el', el);
                    }
                    
                    // get container router
                    const router = container._router;
                    // find after position
                    const before = router.stack.find((item) => {
                        return item.name === 'dataContextMiddleware';
                    });
                    // use diploma-inspector router
                    container.use('/services/diploma-inspector/', diplomaInspectorRouter(app));
                    if (before == null) {
                        // do nothing
                        return;
                    }
                    // get last router
                    const insert = router.stack[router.stack.length - 1];
                    // insert (re-index) router
                    insertRouterBefore(router, before, insert);
                }
            });
        }
    }
}

export {
    DiplomaInspectorService
}