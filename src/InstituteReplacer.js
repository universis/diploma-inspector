import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class InstituteReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Institute');
        const findAttribute = model.fields.find((field) => {
            return field.name === 'dataAvailability'
        });
        if (findAttribute == null) {
            model.fields.push({
                "name": "dataAvailability",
                "type": "DataAvailabilityAttribute",
                "many": true,
                "multiplicity": "ZeroOrOne",
                "mapping": {
                    "associationType": "junction",
                    "associationAdapter": "InstituteDataAvailability",
                    "associationObjectField": "institute",
                    "associationValueField": "dataAvailability",
                    "cascade": "delete",
                    "parentModel": "Institute",
                    "parentField": "id",
                    "childModel": "DataAvailabilityAttribute",
                    "childField": "id",
                    "privileges": [
                        {
                            "mask": 15,
                            "type": "global"
                        },
                        {
                            "mask": 15,
                            "type": "global",
                            "account": "Administrators"
                        },
                        {
                            "mask": 1,
                            "type": "global",
                            "account": "Registrar"
                        }
                    ]
                }
            });
            model.eventListeners = model.eventListeners || [];
            model.eventListeners.push({
                type: path.resolve(__dirname, './listeners/OnBeforeSaveDataAvailability')
            });
            schemaLoader.setModelDefinition(model);
        }
    }

}

export {
    InstituteReplacer
}