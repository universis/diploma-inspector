import {DiplomaInspectorService} from '../src/index';
import { DataApplication, DataCacheStrategy } from '@themost/data';
import path from 'path';
/**
 * @param {DataApplication} app 
 */
async function finalize(app) {
    const cache = app.configuration.getStrategy(DataCacheStrategy);
    if (cache && cache.rawCache) {
        cache.rawCache.flushAll();
        if (typeof cache.rawCache._killCheckPeriod === 'function') {
            cache.rawCache._killCheckPeriod();
        }
    }
}

describe('DiplomaInspectorService', () => {

    it('should create instance', async () => {
        const app = new DataApplication(path.resolve('.'));
        const service = new DiplomaInspectorService(app);
        expect(service).toBeDefined();
        await finalize(app);
    });

});