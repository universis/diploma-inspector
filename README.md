# Universis Diploma Inspector

Degree title verification services for Universis api server 

## Installation

    npm i @universis/diploma-inspector

## Configuration

Register `DiplomaInspectorService` in application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/diploma-inspector#DiplomaInspectorService"
        }
    ]

Add the DiplomaInspectorSchemaLoader under universis api server `schema/loaders` section of application configuration:
```
{
  ...
  "schema": {
    "loaders": [
      ...
      { "loaderType": "@universis/diploma-inspector#DiplomaInspectorSchemaLoader"}
      ...
    ]
  }
  ...
}
```
Use `settings/universis/diploma-inspector` section to configure the usage of diploma inspector services:

     "diploma-inspector": {
        "scope": "diploma-inspector",
        "rateLimit": {
            "windowMs": 300000,
            "max": 5
        },
        "whitelist": [
          "127.0.0.1/24"
        ]
      }



### scope

> string

A string which represents an OAuth2 client scope that is going to be used while authenticating client.

### rateLimit

> object

`@universis/diploma-inspector` uses [express-rate-limit](https://github.com/nfriedly/express-rate-limit#configuration) configuration for limiting the amount of requests against services. The default values are:

      {
          windowMs: 300000, // 5 minutes
          max: 20, // 20 requests
          standardHeaders: true,
          legacyHeaders: false
      }

### whitelist

> string[]

`@universis/diploma-inspector` may be published with restricted access based on an array of ip ranges that be defined under whitelist attribute e.g.

    "whitelist": [
      "127.0.0.1/24"
    ]

Important note: An empty array list disables ip-range check.

## Configure Universis api origin

`settings/universis/api#origin` is being used by `@universis/diploma-inspector` documentation and may be configured if missing:

    "settings": {
      ...
      "universis": {
        ...
        "api": {
          "origin": "https://api.example.com"
        }
      }
    }

## Configure Universis api proxy address forwarding

`settings/universis/api#proxyAddressForwarding` is being used by `@universis/diploma-inspector` in order to safely identify remote clients if universis api is running behind a reverse proxy:

    "settings": {
      ...
      "universis": {
        ...
        "api": {
          "proxyAddressForwarding": true
        }
      }
    }

Note: `settings/universis/api#origin` holds the public address of an instance of universis api server


## Configure OAuth2 client

`@universis/diploma-inspector` router requires an OAuth2 confidential client which uses `client_credentials` grant type.

[https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/](https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/)

